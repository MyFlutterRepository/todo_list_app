import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mocktail/mocktail.dart';
import 'package:todo_list_app/src/features/login/data/datasource/login_datasource.dart';
import 'package:todo_list_app/src/features/login/data/datasource/login_datasource_impl.dart';
import 'package:todo_list_app/src/features/login/data/datasource/login_exceptions.dart';
import 'package:todo_list_app/src/features/login/data/model/login_info_model.dart';

class FirebaseAuthMock extends Mock implements FirebaseAuth {}

class FakeUserCredential extends Fake implements UserCredential {}

class GoogleSignInMock extends Mock implements GoogleSignIn {}

class FakeGoogleSignInAuthentication extends Fake
    implements GoogleSignInAuthentication {
  @override
  String? get accessToken => 'teste';

  @override
  String? get idToken => null;
}

class FakeGoogleSignInAccount extends Fake implements GoogleSignInAccount {
  @override
  Future<GoogleSignInAuthentication> get authentication async =>
      FakeGoogleSignInAuthentication();
}

class FakeOAuthCredential extends Fake implements OAuthCredential {}

void main() {
  late FirebaseAuth auth;
  late GoogleSignIn googleSign;
  late LoginDatasource datasource;

  setUpAll(() {
    registerFallbackValue(FakeOAuthCredential());
  });

  setUp(() {
    auth = FirebaseAuthMock();
    googleSign = GoogleSignInMock();
    datasource = LoginDatasourceImpl(
      auth: auth,
      googleSignIn: googleSign,
    );
  });

  group('LoginDatasource teste -', () {
    group('standardLogin:', () {
      test(
          'should call signInWithEmailAndPassword only one time and concludes without erros',
          () async {
        when(
          () => auth.signInWithEmailAndPassword(
            email: any(named: 'email'),
            password: any(named: 'password'),
          ),
        ).thenAnswer(
          (invocation) async => FakeUserCredential(),
        );

        await datasource.standardLogin(LoginInfoModel(email: '', password: ''));

        verify(
          () => auth.signInWithEmailAndPassword(email: '', password: ''),
        ).called(1);
      });

      test('should throws Exceptions when has erros', () async {
        when(
          () => auth.signInWithEmailAndPassword(
            email: any(named: 'email'),
            password: any(named: 'password'),
          ),
        ).thenThrow(
          UserNotFound(
            message: 'Conta não encontrada',
          ),
        );

        expect(
          () async => await datasource.standardLogin(
            LoginInfoModel(email: '', password: ''),
          ),
          throwsA(isA<UserNotFound>()),
        );

        when(
          () => auth.signInWithEmailAndPassword(
            email: any(named: 'email'),
            password: any(named: 'password'),
          ),
        ).thenThrow(
          EmailOrPassIncorrect(
            message: 'A senha ou e-mail estão errados',
          ),
        );
        expect(
          () async => await datasource.standardLogin(
            LoginInfoModel(email: '', password: ''),
          ),
          throwsA(isA<EmailOrPassIncorrect>()),
        );
      });
    });

    group('standardSignUp:', () {
      test(
          'should call createUserWithEmailAndPassword only one time and concludes without erros',
          () async {
        when(
          () => auth.createUserWithEmailAndPassword(
            email: any(named: 'email'),
            password: any(named: 'password'),
          ),
        ).thenAnswer(
          (invocation) async => FakeUserCredential(),
        );

        await datasource
            .standardSignUp(LoginInfoModel(email: '', password: ''));

        verify(
          () => auth.createUserWithEmailAndPassword(email: '', password: ''),
        ).called(1);
      });

      test('should throws Exceptions when has erros', () async {
        when(
          () => auth.createUserWithEmailAndPassword(
            email: any(named: 'email'),
            password: any(named: 'password'),
          ),
        ).thenThrow(
          WeekPassword(
            message: 'Conta não encontrada',
          ),
        );

        expect(
          () async => await datasource.standardSignUp(
            LoginInfoModel(email: '', password: ''),
          ),
          throwsA(isA<WeekPassword>()),
        );

        when(
          () => auth.createUserWithEmailAndPassword(
            email: any(named: 'email'),
            password: any(named: 'password'),
          ),
        ).thenThrow(
          EmailInUse(
            message: '',
          ),
        );
        expect(
          () async => await datasource.standardSignUp(
            LoginInfoModel(email: '', password: ''),
          ),
          throwsA(isA<EmailInUse>()),
        );
      });
    });

    group('googleLogin:', () {
      test(
          'should call signInWithCredential only one time and concludes without erros',
          () async {
        when(
          () => googleSign.signIn(),
        ).thenAnswer(
          (invocation) async => FakeGoogleSignInAccount(),
        );

        when(
          () => auth.signInWithCredential(any()),
        ).thenAnswer(
          (invocation) async => FakeUserCredential(),
        );

        await datasource.googleLogin();

        verify(
          () => auth.signInWithCredential(any()),
        ).called(1);
      });
      test('should throw an exception when googleUser is null', () async {
        when(
          () => googleSign.signIn(),
        ).thenAnswer(
          (invocation) async => null,
        );

        when(
          () => auth.signInWithCredential(any()),
        ).thenAnswer(
          (invocation) async => FakeUserCredential(),
        );

        expect(
          () async => await datasource.googleLogin(),
          throwsA(isA<LoginError>()),
        );
      });

      test('should throw an exception when has erros', () async {
        when(
          () => googleSign.signIn(),
        ).thenAnswer(
          (invocation) async => FakeGoogleSignInAccount(),
        );

        when(
          () => auth.signInWithCredential(any()),
        ).thenThrow(FirebaseAuthException(code: ''));

        expect(
          () async => await datasource.googleLogin(),
          throwsA(isA<LoginError>()),
        );
      });
    });
  });
}
