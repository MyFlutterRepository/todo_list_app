import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:todo_list_app/src/features/login/data/datasource/login_datasource.dart';
import 'package:todo_list_app/src/features/login/data/model/login_info_model.dart';
import 'package:todo_list_app/src/features/login/data/repositories/login_repository_impl.dart';
import 'package:todo_list_app/src/features/login/domain/entities/login_info.dart';
import 'package:todo_list_app/src/features/login/domain/repositories/login_repository.dart';

class MockLoginDatasource extends Mock implements LoginDatasource {}

class FakeLoginInfoModel extends Fake implements LoginInfoModel {}

void main() {
  final datasource = MockLoginDatasource();
  final data = LoginInfo(email: 'emailTest', password: 'passwordTest');
  late LoginRepository repository;

  setUpAll(() => registerFallbackValue(FakeLoginInfoModel()));

  setUp(() => repository = LoginRepositoryImpl(datasource: datasource));

  group('LoginRepositoryTeste -', () {
    test('loginWithEmail', () async {
      //Arrange
      when(
        () => datasource.standardLogin(any()),
      ).thenAnswer((invocation) async => {});
      //Act
      repository.loginWithEmail(data);
      //Assert
      verify(() => datasource.standardLogin(any())).called(1);
    });

    test('loginWithGoogle', () async {
      //Arrange
      when(
        () => datasource.googleLogin(),
      ).thenAnswer((invocation) async => {});
      //Act
      repository.loginWithGoogle();
      //Assert
      verify(() => datasource.googleLogin()).called(1);
    });

    test('signUpWithEmail', () async {
      //Arrange
      when(
        () => datasource.standardSignUp(any()),
      ).thenAnswer((invocation) async => {});
      //Act
      repository.signUpWithEmail(data);
      //Assert
      verify(() => datasource.standardSignUp(any())).called(1);
    });
  });
}
