import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../features/creation/presentation/pages/new_list_page.dart';
import '../features/creation/presentation/pages/new_task_page.dart';
import '../features/home/presentation/pages/details_page.dart';
import '../features/home/presentation/pages/home_page.dart';
import '../features/login/presentation/pages/splash_page.dart';

final $Routes = GoRouter(
  debugLogDiagnostics: true,
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const SplashPage(),
    ),
    GoRoute(
      path: '/login',
      builder: (context, state) => Container(),
    ),
    GoRoute(
      path: '/home',
      builder: (context, state) => const HomePage(),
      routes: [
        GoRoute(
          path: 'detail',
          builder: (context, state) => const DetailsPage(),
        ),
      ],
    ),
    GoRoute(
      path: '/newList',
      builder: (context, state) => const NewListPage(),
    ),
    GoRoute(
      path: '/newTask',
      builder: (context, state) => const NewTaskPage(),
    ),
  ],
);
