abstract class Failure<T> implements Exception {
  final String message;
  final T? err;

  Failure({
    required this.message,
    this.err,
  });
}
