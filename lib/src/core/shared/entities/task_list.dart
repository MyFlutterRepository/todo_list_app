import 'task.dart';

class TaskList {
  final List<Task> tasks;
  final String listName;
  final String description;
  final int id;

  TaskList({
    required this.tasks,
    required this.listName,
    required this.description,
    required this.id,
  });
}
