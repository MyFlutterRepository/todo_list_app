class Task {
  final String title;
  final String description;
  final bool isDone;
  final int id;
  final DateTime? conclusionTime;

  Task({
    required this.title,
    required this.description,
    required this.isDone,
    required this.id,
    this.conclusionTime,
  });
}
