import 'package:todo_list_app/src/core/shared/entities/task.dart';
import 'package:todo_list_app/src/core/shared/entities/task_list.dart';

abstract class TaskDatasource {
  List<TaskList> getTaskLits([String taskListId = '']);
  void updateTaskDone();
  int createTaskOnList({
    Task task,
    int listId,
  });

  int createNewList(String listName, String description);
}
