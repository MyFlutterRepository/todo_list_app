import '../../../../core/shared/entities/task.dart';

abstract class TaskCreationRepository {
  int createTaskOnList({
    Task task,
    int listId,
  });

  int createNewList(String listName, String description);
}
