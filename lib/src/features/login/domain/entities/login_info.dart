class LoginInfo {
  final String email;
  final String password;

  LoginInfo({
    required this.email,
    required this.password,
  });
}
