import '../entities/login_info.dart';

abstract class LoginRepository {
  Future<void> loginWithGoogle();
  Future<void> loginWithEmail(LoginInfo info);
  Future<void> signUpWithEmail(LoginInfo info);
}
