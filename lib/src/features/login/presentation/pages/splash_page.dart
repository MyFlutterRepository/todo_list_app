import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:mwidgets/mwidgets.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  late final AnimationController controller;
  @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 800),
      lowerBound: 0.2,
      upperBound: 1,
    );

    super.initState();
    Future.delayed(
      const Duration(milliseconds: 200),
    ).then(
      (value) => controller.forward().then(
            (value) => context.go('/home'),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ScaleTransition(
            scale: controller.view,
            child: MLogoTodoApp.standard(behaviour: Behaviour.regular),
          ),
          AnimatedBuilder(
            animation: controller,
            builder: (context, _) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: MSizes.s32),
                child: LinearProgressIndicator(
                  value: controller.value,
                  color: MColors.blue1,
                  backgroundColor: MColors.background2,
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
