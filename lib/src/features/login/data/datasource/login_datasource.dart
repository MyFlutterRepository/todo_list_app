import '../model/login_info_model.dart';

abstract class LoginDatasource {
  /// Can throw UserNotFound, EmailOrPassIncorrect exceptions
  Future<void> standardSignUp(LoginInfoModel info);

  /// Can throw WeekPassword, EmailInUse exceptions
  Future<void> standardLogin(LoginInfoModel info);

  /// Can throw LoginError exceptions
  Future<void> googleLogin();
}
