import 'package:todo_list_app/src/core/shared/entities/failure.dart';

class UserNotFound extends Failure {
  UserNotFound({required super.message, super.err});
}

class EmailOrPassIncorrect extends Failure {
  EmailOrPassIncorrect({required super.message, super.err});
}

class WeekPassword extends Failure {
  WeekPassword({required super.message, super.err});
}

class EmailInUse extends Failure {
  EmailInUse({required super.message, super.err});
}

class LoginError extends Failure {
  LoginError({required super.message, super.err});
}
