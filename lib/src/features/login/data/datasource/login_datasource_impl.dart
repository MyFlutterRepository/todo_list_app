import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:todo_list_app/src/features/login/data/model/login_info_model.dart';

import 'login_datasource.dart';
import 'login_exceptions.dart';

class LoginDatasourceImpl implements LoginDatasource {
  FirebaseAuth auth;
  GoogleSignIn googleSignIn;

  LoginDatasourceImpl({
    required this.auth,
    required this.googleSignIn,
  });

  @override
  Future<void> standardLogin(LoginInfoModel info) async {
    try {
      await auth.signInWithEmailAndPassword(
        email: info.email,
        password: info.password,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw UserNotFound(
          message: 'Conta não encontrada',
        );
      } else if (e.code == 'wrong-password') {
        throw EmailOrPassIncorrect(message: 'A senha ou e-mail estão errados');
      }
    }
  }

  @override
  Future<void> standardSignUp(LoginInfoModel info) async {
    try {
      await auth.createUserWithEmailAndPassword(
        email: info.email,
        password: info.password,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        throw WeekPassword(message: 'A sua senha precisa ser mais forte.');
      } else if (e.code == 'email-already-in-use') {
        throw EmailInUse(message: 'Esta conta ja está em uso.');
      }
    }
  }

  @override
  Future<void> googleLogin() async {
    try {
      final googleUser = await googleSignIn.signIn();

      final googleAuth = await googleUser?.authentication;

      if (googleAuth?.accessToken == null && googleAuth?.idToken == null) {
        throw Exception('google auth null');
      }

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );

      await auth.signInWithCredential(credential);
    } on Exception catch (e) {
      throw LoginError(
        message: "Erro ao logar com o google",
        err: e,
      );
    }
  }
}
