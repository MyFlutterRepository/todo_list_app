import 'package:todo_list_app/src/features/login/data/datasource/login_datasource.dart';
import 'package:todo_list_app/src/features/login/data/model/login_info_model.dart';
import 'package:todo_list_app/src/features/login/domain/entities/login_info.dart';

import '../../domain/repositories/login_repository.dart';

class LoginRepositoryImpl implements LoginRepository {
  LoginDatasource datasource;

  LoginRepositoryImpl({
    required this.datasource,
  });

  @override
  Future<void> loginWithEmail(LoginInfo info) async {
    await datasource.standardLogin(LoginInfoModel.fromEntity(info));
  }

  @override
  Future<void> loginWithGoogle() async {
    await datasource.googleLogin();
  }

  @override
  Future<void> signUpWithEmail(LoginInfo info) async {
    await datasource.standardSignUp(LoginInfoModel.fromEntity(info));
  }
}
