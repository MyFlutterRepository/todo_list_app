import 'package:todo_list_app/src/features/login/domain/entities/login_info.dart';

class LoginInfoModel extends LoginInfo {
  LoginInfoModel({
    required super.email,
    required super.password,
  });

  factory LoginInfoModel.fromEntity(LoginInfo entity) {
    return LoginInfoModel(email: entity.email, password: entity.password);
  }
}
