import 'package:flutter/material.dart';
import 'package:mwidgets/mwidgets.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  final _staticBehaviour = Behaviour.regular;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MLabel.h2SemiboldRobot(
          behaviour: _staticBehaviour,
          text: "Todas as listas",
        ),
        foregroundColor: Colors.white,
        backgroundColor: MColors.blue2,
        actions: [
          PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                child: MButton.textH1RegularRoboto(
                  behaviour: _staticBehaviour,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
