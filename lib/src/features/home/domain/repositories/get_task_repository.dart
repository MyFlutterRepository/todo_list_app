import '../../../../core/shared/entities/task.dart';
import '../../../../core/shared/entities/task_list.dart';

abstract class GetTasksRepository {
  List<Task> getTaskList(int taskListId);
  List<TaskList> getAllList();
  void updateTaskDone({
    int taskId,
    bool isDone,
  });
}
