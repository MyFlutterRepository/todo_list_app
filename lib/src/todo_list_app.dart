import 'package:flutter/material.dart';

import 'core/routes.dart';

class TodoList extends StatelessWidget {
  const TodoList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: $Routes,
    );
  }
}
